import React from 'react';
import ReactDOM from 'react-dom/client';

import App from './App';

//bstrap
//index.js - rootfile for react; render file or tech
import 'bootstrap/dist/css/bootstrap.min.css'



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


//javascript xml - we dont need html file, so in order to satisfy html elements  use xml
//react has builtin live server
// const name = 'Stark';

// const user = {
//   firstName: 'Jane',
//   lastName: 'Smith'
// }

// const formatName = (user) => {
//   return `${user.firstName} ${user.lastName}`
// }

// const element = <h1> Hello, {formatName(user)} </h1>

// //method like dom, located inside index.html
// //update of react createRoot()
// const root = ReactDOM.createRoot(document.getElementById('root'))

// root.render(
//     element
//   )

// ReactDOM.render(
//   element,
//   document.getElementById('root')
// )

