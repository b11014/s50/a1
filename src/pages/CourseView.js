import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useParams} from 'react-router-dom';
//will use id
import UserContext from '../UserContext';




export default function CourseView(){
//consume user here; if logged user can enroll
 	const {user} = useContext(UserContext);

	const {courseId} = useParams();


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);


//take courseid as parameter
	const enroll = (courseId) => {

		fetch('http://localhost:4000/users/enroll', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`

			},
			body: JSON.stringify({
				courseId: courseId 
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
		})
	}

	useEffect(() => {
		console.log(courseId)
		// usebackticks
		fetch(`http://localhost:4000/courses/getSingleCourse/${courseId}`)
		
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name)
			setPrice(data.price)
			setDescription(data.description)
		

		})

	}, [courseId])

	return(
		<Container className="mt-5">
		<Row>
			<Col lg={{span:6, offset: 3}}>
			<Card>
				<Card.Body>
				<Card.Title>{name}</Card.Title>

				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>

				<Card.Subtitle>Price</Card.Subtitle>
				<Card.Text>{price}</Card.Text>

				<Card.Subtitle>Class Schedule</Card.Subtitle>
				<Card.Text>8 AM to 5 PM</Card.Text>

				<Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
				</Card.Body>

			</Card>
			</Col>
		</Row>
		</Container>
	)
}