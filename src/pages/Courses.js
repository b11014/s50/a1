// import coursesData from '../data/coursesData';
import {useEffect, useState} from 'react'
import CourseCard from '../components/CourseCard';
export default function Courses() {
	// console.log(coursesData);
	// console.log(coursesData[0]); //to check the first element

	//iteration
	//hold data, coursesData is array of objects can use array method
	// map()creates new array, course here iterated course
	//key can be a paramter
	// const courses = coursesData.map(course => {
	// 	console.log(course.id);
	// 	return (
			// key - react have key to all components; unique identifier , iterated course
			//coursecard is an object , key identify what is the elements inside map method, thats why it iterated because each course has a unique id, use key for individuality
			//map method has a part with key
			//so for iteration they can have individual id
		// 		<CourseCard key = {course.id} courseProp = {course}/>
		// 	)
		// })
// fetch is retrieval only

const [courses, setCourses] = useState([]) 
		useEffect(() => {
			fetch('http://localhost:4000/courses')
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setCourses(data.map(course => {
					return(
						<CourseCard key={course._id} courseProp={course}/>
						)
				}))
			})

		}, [])


	return(
		<>
		<h1> Available Courses: </h1>
			{/*props - shorter term for properties = value; certain value can be use or pass in different components */}
			{/*<CourseCard courseProp = {coursesData[0]}/>*/}
		{/*<CourseCard courseProp = {coursesData[0].price}/>*/}
				{courses}
		</>
		)
}