import {Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


// So hooks are from react and always have a value"use"
// hook of react shorten the codes

export default function Login(props) {
	console.log(props);

//consume login set to local storage, chnages sa value ng user
	const {user, setUser} = useContext(UserContext);
	console.log(user);
	
	const [email, loginEmail] = useState('');
	const [password, loginPassword] = useState('');
	const [isActive, loginActive] = useState(false);

	console.log(email);
	console.log(password);

	useEffect(() => {

			if(email !== '' && password !== ''){
				loginActive(true);
			}else{
				loginActive(false);
			}

	},[email, password]);



// 
// backend route (url, options)
function loginUser(e) {
		e.preventDefault();


		fetch('http://localhost:4000/users/login', {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)
//you can change structure base
				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Booking App of 182!'
				})
			}else{
				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Check your credentials'
				})
			}
		})


		// localStorage.setItem('email', email)

		// setUser({
		// 	email: localStorage.getItem('email')
		// });

		loginEmail('');
		loginPassword('');

		// alert('Thanks for logging in!')
	
}

//fetch=get
const retrieveUserDetails = (token) => {
 	fetch('http://localhost:4000/users/getUserDetails', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        })

	//database and frontend
	.then(res => res.json())
	//retrieve data from fetch
	.then(data => {
		console.log(data)

		setUser({
			_id: data._id,
			isAdmin: data.isAdmin
			// verification of login, id=who is the user logged in, to know the application wht to display it need to know what kind of user is logged in
		})


	})
}


// form

return (

	(user.id !== null)?
	<Navigate to="/courses"/>
	:
	<>
		<h1>Login Here:</h1>
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group controlId="userEmail_login">
				<Form.Label>Email Address: </Form.Label>
				<Form.Control
					type="email"
					placeholder="name@example.com" required
						value={email}
						onChange={e => loginEmail(e.target.value)}
				/>
			</Form.Group>


{/*password*/}
	<Form.Group controlId="password">
				<Form.Label>Password: </Form.Label>
				<Form.Control
					type="password"
					placeholder="Password here."
						required
						value={password}
						onChange={e => loginPassword(e.target.value)}
					/>
			</Form.Group>



		{isActive ?

			<Button variant="primary" type="submit" className="mt-3 mb-5" id="loginBtn">Login
			</Button>
			:
			<Button variant="secondary" type="submit" className="mt-3 mb-5" disabled>Login
			</Button>
		}
		</Form>
	</>
	)

}