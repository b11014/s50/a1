import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';
export default function NotFound() {
	return (
		//jumbtron/banner
		//color = variant
		<Row>
			<Col className = "p-5">
				<h1>404 - Not Found</h1>
				<p>Return to <Button variant="primary" as={Link} to="/">HOME PAGE</Button> </p>

				
				
			</Col>

		</Row>
		)
}