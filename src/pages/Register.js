import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';


//how does react handles form
export default function Register() {

	// consume user, destruture, since user has been consume from user context, you can now use users email
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	const history = useNavigate();
	console.log(email);
	console.log(password);

// for button change into active
	// useEffect(() => {
	// 	if((email !== '' && password !== '' && password1 !== '') && (password === password1)){

	// 		setIsActive (true);
	// 	}else{
	// 		setIsActive (false);

	// 	}
	// }, [email, password, password1]);



	useEffect(() => {
		if(firstName !== '' && lastName !== '' && mobileNo.length === 11 && email !== '' && password !== ''){

			setIsActive(true);
		
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, mobileNo, email, password]);




//function to clear out fields after registration, no reloading of page
	
	//pass the event then call function
function registerUser(e) {

		e.preventDefault();

		fetch('http://localhost:4000/users/checkEmailExists', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: 'Duplicate email found',
					icon: 'info',
					text: 'Please provide another email'
				})
			
			} else {

				fetch('http://localhost:4000/users', {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data.email){

						Swal.fire({
							title: 'Registration successful',
							icon: 'success',
							text: 'Thank you for registering'
						})

						history("/login")
					
					} else {

						Swal.fire({
							title: 'Registration failed',
							icon: 'error',
							text: 'Something went wrong'
						})
					}
				})
			}
		})

		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword('');
	}

	
	return (
		user.id !== null ?
		<Navigate to="/courses"/>
		:
		<>
		<h1>Register Here:</h1>
		<Form onSubmit={e => registerUser(e)}>

			<Form.Group controlId="firstName">
				<Form.Label>First Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please input your first name here"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please input your last name here"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control
					type="text"
					placeholder="Please input your 11-digit mobile number here"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
				/>
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label>Email Address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Please input your email here"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password:</Form.Label>
				<Form.Control
					type="password"
					placeholder="Please input your password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>

		{ isActive ?
			<Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5" >
				Register
			</Button>

			:
			
			<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-5" disabled>
				Register
			</Button>

		}
		
		</Form>
		</>

	)
}

