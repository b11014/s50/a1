const mockData = [
	{
		email: "user@example.com",
		password: "user123"
	},
	{
		email: "user1@example.com",
		password: "user1234"
	}
]

export default mockData;