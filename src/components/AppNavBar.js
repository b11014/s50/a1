import {useContext} from 'react';
//import main tags
import {Navbar, Container, Nav} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext'
//pascal casing, starts with capital letter, naming convention

//two type of react
//function 
//es6 , no 2 funtion components sa isang file
//class default class AppNavBar extends Components {

export default function AppNavBar() {

//need user context
//usecontext is a react hook that consumes the value
// store info
//we need to destructure to user certain objects

  const {user} = useContext(UserContext);
  console.log(user);


	return (
	<Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/">Booking App</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link  as={Link} to="/courses">Courses</Nav.Link>

            {
              //render navigation bar
              (user.id !== null) ?
              <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
              :
              <>
              <Nav.Link  as={Link} to="/login">Login</Nav.Link>
                        <Nav.Link  as={Link} to="/register">Register</Nav.Link>
                        </>}
          
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
	)
}