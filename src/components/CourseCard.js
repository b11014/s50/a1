import {Card} from 'react-bootstrap';
// import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';



//declare parameter as objects
export default function CourseCard({courseProp}) {
    console.log(courseProp);
    //can be multiple parameters props,price
    // console.log(props); //coursesData[0] - wdc001
    // console.log(typeof props); //object

    //object destructure; destructure in parameter the name of the props in Courses.js insrt in curly braces
    const {name, description, price, _id} = courseProp

    console.log(name);

    // getter - first to handle the initial state, value
    // setter - make the changes to setter, function
    //changes will take in use change
    //** one value-getter, take initial value, one setter, holds the possible changes of getter
    //useState can have different data type
    // const [count, setCount] = useState(0)
    // const [seat, setSeat] = useState(30)
    // const [isOpen, setIsOpen] = useState(false)

    // const [name, setName] = useState("")
    // const enroll = () => {
    //     setCount(count + 1)
    //     console.log(`Enrollees: ${count}`)

    //     setSeat(seat - 1)
    //     if(seat === 0){
    //         alert('No more seats')
    //     }
    //     return;
    // }

    // const enroll = () => {
    //     if(seat === 0 && count === 30){
    //     alert('No more seats')
    //     }else{
    //         setCount(count + 1)
    //         setSeat(seat - 1)
    //     };
    // }

    // const enroll = () => {
    //     if(seat 0) {
    //         setCount(count + 1)
    //         setSeat(seat - 1)
    //     }else{
    //         alert("No more seats available.")
    //     };
    // }

    // useEffect(() => {
    //     if(seat === 0){
    //         setIsOpen(true)
    //         alert('No more seats')
    //     }
    // }, [seat])
    //[seat] where changes happened
    //optional parameters

    // use effect will take effect when changes happened, currentdom
    //virtual dom -- apply changes, reload changes in a specific part of the web where the processed happened


   
    //concept of use state
    // usestate - syntax : const [getter,setter] =useState(initialValueOfGetter)
    //a react hook, shortened method; logic part


    //jxs allows add html in js
	 return(
    <Card className= "mb-4">
            <Card.Body>
        {/*since property is destructured, insert in curlybraces, jslogic-react*/}

                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>


         {/*    <Card.Text>Enrollees: {count}</Card.Text>
             <Card.Text>Seats: {seat}</Card.Text>*/}
             {/*   <Button variant="primary" onClick = {enroll} disabled={isOpen}>Enroll</Button>*/}

            <Link className="btn btn-primary" to={`/courseView/${_id}`}>View Details</Link>
            </Card.Body>
        </Card>

	)
}