import './App.css';
import {Container} from 'react-bootstrap';
//each have their own purpose, router-is an alias
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
;
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home'
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';
import {UserProvider} from './UserContext';
import {useState, useEffect} from 'react';
//all imported dependencies need to be imported


//context will come here; accessing
function App() {
//logged in w input
      const [user, setUser] = useState({
        // email: localStorage.getItem('email')
        id: null,
        isAdmin: null
    })


//logout, clear  storage 
      const unsetUser = () => {
        localStorage.clear()
  }

  // useEffect(() => {
  //   // if theres a changes, what data user take and in stoorage
  //   console.log(user); //_id
  //   console.log(localStorage); //accesstoken
  // }, [user])


  useEffect(() => {
    fetch('http://localhost:4000/users/getUserDetails', {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      // captured the data of whoever is logged in
      console.log(data)
          
          // set the user states values with the user details upon successful login
          if(typeof data._id !== "undefined"){
              setUser({
                  id: data._id,
                  isAdmin: data.isAdmin
              })
          } else {

            // set back the initial state of user
              setUser({
                  id: null,
                  isAdmin: null
              })
          }
      })

  }, [])





  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router> 
    <AppNavBar/>
    <Container>
      <Routes>
           <Route exact path="/" element={<Home/>}/>
           <Route exact path="/courses" element={<Courses/>}/>
           <Route exact path="/courseView/:courseId" element={<CourseView/>} />
           <Route exact path="/register" element={<Register/>}/>
           <Route exact path="/login" element={<Login/>}/>
           <Route exact path="/logout" element={<Logout/>}/>
           <Route exact path="*" element={<NotFound/>}/>
        </Routes>
    </Container>
    </Router>
    </UserProvider>
    // since this are needed by user wrap with the right component
  );
}

export default App;

// router will be the parent of routes, instead of <>
// routes will group components that we're going to use; <Switch> before update
//route will be the specific path <cases>